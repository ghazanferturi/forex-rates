# ForexRates - v1.0.0
Currency Rates using Symfony (PHP)

## Author
- Ghazanfar Abbas

## Technology Stack
- PHP
  - Symfony 4
  - Twig Template Engine
  - Bootstrap 4.0
  - Ajax DataTables

## Requirements
- PHP 7.1
- Composer
- Exchange Rates API
    - https://exchangeratesapi.io/
    - Exchange rates API is a free service for current and historical foreign exchange rates
      published by the European Central Bank

## Setup
- Clone project & Change to project directory
- Navigate to project directory and give write access to project directory:
    - Run `$ cd forex-rates` navigate to project directory
    - Run `$ sudo chmod -R 777 ./` give write access so that composer install files.
    - Start docker container `docker-compose up -d`.
    - Log in to the PHP7 image: `docker-compose exec php-fpm bash`
- Run `composer install` insider project directory to install required symfony web-server dependencies.
- Run `$ php bin/console server:run 127.0.0.1:9000` to view in local browser
    - Browse to `http://127.0.0.1:9000` and ensure you receive the home page.