<?php

namespace App\Controller;

use App\Entity\ExchangeEnquiry;
use App\Form\Type\ExchangeEnquiryType;
use App\WebService\ConversionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeRateController extends AbstractController
{
    /**
     * @Route("/exchange/rate", name="exchange_rate")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('exchange_rate/index.html.twig', [
            'controller_name' => 'ExchangeRateController',
        ]);
    }

    /**
     * @Route("/exchange/convert", name="convert_response")
     * @param Request $request
     * @param ConversionService $service
     * @return Response
     */
    public function convert(Request $request, ConversionService $service): Response
    {
        $data = new ExchangeEnquiry();
        $form = $this->createForm(ExchangeEnquiryType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rates = $service->getHistoryOfLastTenConversion($data->getBaseCurrency());
            unset($rates[$data->getBaseCurrency()]);

            return $this->render('exchange_rate/response.html.twig', [
                'enquiry' => $data,
                'result' => $service->convert($data),
                'rates' => $rates
            ]);
        }

        return $this->render('exchange_rate/input.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
