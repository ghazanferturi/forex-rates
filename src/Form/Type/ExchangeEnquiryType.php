<?php

namespace App\Form\Type;

use App\Entity\ExchangeEnquiry;
use App\WebService\ConversionService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExchangeEnquiryType extends AbstractType
{
    /**
     * @var ConversionService
     */
    private $conversionService;

    /**
     * ExchangeEnquiryType constructor.
     * @param ConversionService $conversionService
     */
    public function __construct(ConversionService $conversionService)
    {
        $this->conversionService = $conversionService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $supportedCurrencies = $this->conversionService->getSupportedCurrencies();

        $builder
            ->add('amount', TextType::class)
            ->add('baseCurrency', ChoiceType::class,
                [
                    'placeholder' => 'Choose Base Currency',
                    'choices' => $supportedCurrencies
                ]
            )
            ->add('targetCurrency', ChoiceType::class,
                [
                    'placeholder' => 'Choose Target Currency',
                    'choices' => $supportedCurrencies
                ]
            )
            ->add('Convert', SubmitType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExchangeEnquiry::class
        ]);
    }

}