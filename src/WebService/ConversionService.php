<?php


namespace App\WebService;

use App\Entity\ExchangeEnquiry;
use App\WebService\Exchange\ExchangeInterface;

class ConversionService
{
    /**
     * @var ExchangeInterface
     */
    protected $exchange;

    /**
     * @var CalculateExchangeInterface
     */
    protected $converter;

    /**
     * ConversionService constructor.
     * @param ExchangeInterface $exchange
     * @param CalculateExchangeInterface $converter
     */
    public function __construct(ExchangeInterface $exchange,  CalculateExchangeInterface $converter)
    {
        $this->exchange = $exchange;
        $this->converter = $converter;
    }

    /**
     * @param string $baseCurrency
     * @param string $targetCurrency
     * @return float
     */
    public function getExchangeRate(string $baseCurrency, string $targetCurrency): float
    {
        return $this->exchange->getLatestExchangeRate($baseCurrency, $targetCurrency);
    }

    /**
     * @param string $baseCurrency
     * @return array
     */
    public function getHistoryOfLastTenConversion(string $baseCurrency): array
    {
        return $this->exchange->getHistoryOfLastTenConversion($baseCurrency);
    }

    /**
     * @param ExchangeEnquiry $enquiry
     * @return float
     */
    public function convert(ExchangeEnquiry $enquiry): float
    {
        $exchangeRate = $this->exchange->getLatestExchangeRate(
            $enquiry->getBaseCurrency(),
            $enquiry->getTargetCurrency()
        );

        return $this->converter->Convert($enquiry->getAmount(), $exchangeRate);
    }

    /**
     * @return array
     */
    public function getSupportedCurrencies(): array
    {
        return $this->exchange->getSupportedCurrencies();
    }
}