<?php


namespace App\WebService;


class CalculateExchange implements CalculateExchangeInterface
{
    /**
     * @inheritDoc
     */
    public function convert(float $baseAmount, float $exchangeRate): float
    {
        return $baseAmount * $exchangeRate;
    }
}