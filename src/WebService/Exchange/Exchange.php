<?php

namespace App\WebService\Exchange;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Exchange implements ExchangeInterface
{
    protected const REQUEST_METHOD = 'GET';

    /**
     * @var string
     */
    protected $baseUri;

    /**
     * Exchange constructor.
     * @param string $baseUri
     */
    public function __construct(string $baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @inheritDoc
     */
    public function getLatestExchangeRate(string $baseCurrency, string $targetCurrency): float
    {
        if ($baseCurrency === $targetCurrency) {
            return 1.0;
        }

        $client = HttpClient::create();
        $rates = null;
        try {
            $response = $client->request(
                static::REQUEST_METHOD,
                $this->baseUri . '/latest', [
                'query' => [
                    'base' => $baseCurrency
                ]
            ]);
            $responseArray = $response->toArray();
            $rates = $responseArray['rates'];
        } catch (ClientExceptionInterface $e) {
        } catch (DecodingExceptionInterface $e) {
        } catch (RedirectionExceptionInterface $e) {
        } catch (ServerExceptionInterface $e) {
        } catch (TransportExceptionInterface $e) {
        }

        return (float) $rates[$targetCurrency];
    }

    /**
     * @param string $baseCurrency
     * @return array
     */
    public function getHistoryOfLastTenConversion(string $baseCurrency): array
    {
        $client = HttpClient::create();
        $responseArray = null;
        try {
            $response = $client->request(
                static::REQUEST_METHOD,
                $this->baseUri . '/latest', [
                'query' => [
                    'base' => $baseCurrency
                ]
            ]);
            $responseArray = $response->toArray();
        } catch (ClientExceptionInterface $e) {
        } catch (DecodingExceptionInterface $e) {
        } catch (RedirectionExceptionInterface $e) {
        } catch (ServerExceptionInterface $e) {
        } catch (TransportExceptionInterface $e) {
        }

        return $responseArray['rates'];
    }

    /**
     * @inheritDoc
     */
    public function getSupportedCurrencies(): array
    {
        return [
            'CAD' => 'CAD','HKD' => 'HKD','ISK' => 'ISK','PHP' => 'PHP',
            'DKK' => 'DKK','HUF' => 'HUF','CZK' => 'CZK','GBP' => 'GBP',
            'RON' => 'RON','SEK' => 'SEK','IDR' => 'IDR','INR' => 'INR',
            'BRL' => 'BRL','RUB' => 'RUB','HRK' => 'HRK','JPY' => 'JPY',
            'THB' => 'THB','CHF' => 'CHF','EUR' => 'EUR','MYR' => 'MYR',
            'BGN' => 'BGN','TRY' => 'TRY','CNY' => 'CNY','NOK' => 'NOK',
            'NZD' => 'NZD','ZAR' => 'ZAR','USD' => 'USD','MXN' => 'MXN',
            'SGD' => 'SGD','AUD' => 'AUD','ILS' => 'ILS','KRW' => 'KRW',
            'PLN' => 'PLN'
        ];
    }
}