<?php


namespace App\WebService\Exchange;


interface ExchangeInterface
{
    /**
     * @param string $baseCurrency
     * @param string $targetCurrency
     *
     * @return float
     */
    public function getLatestExchangeRate(string $baseCurrency, string $targetCurrency): float;

    /**
     * @param string $baseCurrency
     * @return array
     */
    public function getHistoryOfLastTenConversion(string $baseCurrency): array;

    /**
     * @return array
     */
    public function getSupportedCurrencies(): array;
}