<?php


namespace App\WebService;


interface CalculateExchangeInterface
{
    /**
     * @param float $baseAmount
     * @param float $exchangeRate
     *
     * @return float
     */
    public function convert(float $baseAmount, float $exchangeRate): float;
}