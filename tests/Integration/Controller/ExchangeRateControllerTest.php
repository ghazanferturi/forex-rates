<?php


namespace App\Tests\Integration\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExchangeRateControllerTest extends WebTestCase
{
    public function testResponse()
    {
        $amount         = 123.45;
        $targetCurrency = 'USD';
        $client         = static::createClient();
        $crawler        = $client->request('GET', '/exchange/convert');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}