<?php

namespace App\Tests\Unit\WebService;

use App\Entity\ExchangeEnquiry;
use App\WebService\CalculateExchange;
use App\WebService\ConversionService;
use App\WebService\Exchange\ExchangeInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ConversionServiceTest extends TestCase
{
    /**
     * @var ExchangeInterface
     */
    protected $exchange;

    protected function setUp(): void
    {
        $this->exchange = $this->prophesize()->willImplement(ExchangeInterface::class);
    }

    /**
     * @param string $baseCurrency
     * @param string $targetCurrency
     * @param float $exchangeRate
     *
     * @dataProvider getExchangeRateProvider
     */
    public function testGetExchangeRate(string $baseCurrency, string $targetCurrency, float $exchangeRate)
    {
        $this->exchange
            ->getLatestExchangeRate($baseCurrency, $targetCurrency)
            ->willReturn($exchangeRate);

        $conversionServiceMock = $this
            ->getMockBuilder(ConversionService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertSame($exchangeRate, $conversionServiceMock->getExchangeRate($baseCurrency, $targetCurrency));
    }

    /**
     * @param ExchangeEnquiry $enquiry
     * @param float $exchangeRate
     *
     * @dataProvider exchangeDataProvider
     */
    public function testConversion(ExchangeEnquiry $enquiry, float $exchangeRate)
    {
        $this->exchange
            ->getLatestExchangeRate($enquiry->getBaseCurrency(), $enquiry->getTargetCurrency())
            ->willReturn($exchangeRate);

        $conversionServiceMock = $this
            ->getMockBuilder(ConversionService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $convertedAmount = $conversionServiceMock->convert($enquiry);
        $this->assertSame($enquiry->getAmount() * $exchangeRate, $convertedAmount);
    }

    /**
     * @return array
     */
    public function getExchangeRateProvider(): array
    {
        return [
            [
                'baseCurrency'   => 'EUR',
                'targetCurrency' => 'PLN',
                'exchangeRate'   => 0.0,
            ],
            [
                'baseCurrency'   => 'USD',
                'targetCurrency' => 'CAD',
                'exchangeRate'   => 0.0,
            ],
        ];
    }

    /**
     * @return array
     */
    public function exchangeDataProvider(): array
    {
        return [
            [
                'enquiry'      => new ExchangeEnquiry(
                    0.00,
                    'EUR',
                    'PLN'
                ),
                'exchangeRate' => 4.2,
            ],
            [
                'enquiry'      => new ExchangeEnquiry(
                    0.00,
                    'CAD',
                    'USD'
                ),
                'exchangeRate' => 0.81,
            ],
            [
                'enquiry'      => new ExchangeEnquiry(
                    null,
                    null,
                    null
                ),
                'exchangeRate' => 4.2,
            ],
        ];
    }


}