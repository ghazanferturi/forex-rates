<?php

namespace App\Tests\Unit\WebService;

use App\WebService\CalculateExchange;
use PHPUnit\Framework\TestCase;


class ConversionTest extends TestCase
{
    /**
     * @param float $baseAmount
     * @param float $exchangeRate
     * @param float $expectedAmount
     *
     * @dataProvider conversionDataProvider
     */
    public function testConvert(float $baseAmount, float $exchangeRate, float $expectedAmount)
    {
        $converter = new CalculateExchange();

        $this->assertSame($expectedAmount, $converter->convert($baseAmount, $exchangeRate));
    }

    /**
     * @return array
     */
    public function conversionDataProvider(): array
    {
        return [
            [
                'baseAmount'     => 100,
                'exchangeRate'   => 2,
                'expectedAmount' => 200,
            ],
            [
                'baseAmount'     => 7000,
                'exchangeRate'   => 0.24064,
                'expectedAmount' => 1684.48,
            ],
            [
                'baseAmount'     => 164.4987651898,
                'exchangeRate'   => 9.46998438,
                'expectedAmount' => 1557.800736876693735,
            ],
        ];
    }
}